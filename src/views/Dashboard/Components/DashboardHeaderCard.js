
import React, { Component } from "react";

 function DashboardHeaderCard(props) {
   return (
     <div className="col-lg mb-4 col-sm-6 col-md-6">
      <div className="card" style={{padding:20,height:"25rem"}}>
         <div className="card-body">
         
         <div className="stat-heading" style={{textAlign:'left',marginBottom:50,fontSize:30}}>{props.title}</div>
         
         <div className="text-left" style={{float:'left'}}>
         <div className="stat-text">
             <span className="count">
             <i
                 className={`fa fa-${props.icon1} ${props.color1} fa-4x`}
                 aria-hidden="true"
               >
               {props.count1}
               </i></span>
             </div>
         <div className="stat-heading" style={{textAlign:'center'}}>{props.info1}</div>
           
          </div>
          
 
          
         <div className="text-right"  style={{float:'right'}}>
         <div className="stat-text">
         <span className="count">
             <i
                 className={`fa fa-${props.icon2} ${props.color2} fa-4x`}
                 aria-hidden="true"
               >
               {props.count2}
               </i></span>
             </div>
         <div className="stat-heading" style={{textAlign:'center'}}>{props.info2}</div>
         
          </div>
          
         </div>
       </div>
     </div>
   );
 }
 
 export default DashboardHeaderCard;
